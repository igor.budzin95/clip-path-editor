import React, { Fragment } from 'react'
import Head from 'next/head'
import type { AppProps } from 'next/app'
import 'styles/styles.scss'

function App({ Component, pageProps }: AppProps) {
    return (
        <Fragment>
            <Head>
                <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            </Head>
         
            <div className="page-container">
                <Component {...pageProps} />
            </div>
        </Fragment>
    )
}

export default App