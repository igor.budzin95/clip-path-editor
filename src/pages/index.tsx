import Head from 'next/head'
import { Fragment } from 'react'
import Poligon from 'components/Poligon/Poligon'
import CodeDashboard from 'components/CodeDashboard/CodeDashboard'
import Sidebar from "components/Sidebar/Sidebar";


export default function Home() {
    return (
        <Fragment>
            <div className="container">
                <div className="row">
                    <div className="col-9">
                        <div className="d-flex flex-column">
                            <Poligon  />

                            <div className="m-t-20">
                                <CodeDashboard />
                            </div>
                        </div>
                    </div>
                    <div className="col-3">
                        <Sidebar />
                    </div>
                </div>
            </div>
        </Fragment>
    )
}
