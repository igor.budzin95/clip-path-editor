import React from 'react'
import { observer } from 'mobx-react'
import MarkersStore from 'stores/MarkersStore'



const Sidebar = props => {
    const { markers, activeMarkerID } = MarkersStore

    const onUpdateCoordinate = (axis, value) => {
        if(value > 100) value = 100
        else if(value < 0) value = 0

        MarkersStore.updateMarker({
            id: activeMarkerID,
            [axis]: parseInt(value, 10)
        })
    }

    return (
        <aside className="sidebar">

            <div className="card m-b-20">
                <div className="card-header">Editor</div>
                <div className="card-body">

                    <div className="sidebar-color m-b-20">
                        Color: <span className="sidebar-color__rect" style={{ background: markers.filter(el => el.id === activeMarkerID)[0]?.color || "none" }}></span>
                        {markers.filter(el => el.id === activeMarkerID)[0]?.color || ""}
                    </div>

                    <div className="row">
                        <div className="col-6">

                            <label htmlFor="input-x">Coordinate by <b>X</b></label>
                            <div className="input-group mb-3">
                                <input
                                    type="number"
                                    min="0"
                                    max="100"
                                    step="1"
                                    className="form-control"
                                    id="input-x"
                                    value={markers.filter(el => el.id === activeMarkerID)[0]?.x || ""}
                                    onChange={event => onUpdateCoordinate('x', event.target.value)}
                                />
                                <div className="input-group-append">
                                    <span className="input-group-text">%</span>
                                </div>
                            </div>

                        </div>
                        <div className="col-6">

                            <label htmlFor="input-y">Coordinate by <b>Y</b></label>
                            <div className="input-group mb-3">
                                <input
                                    type="number"
                                    min="0"
                                    max="100"
                                    step="1"
                                    className="form-control"
                                    id="input-y"
                                    value={markers.filter(el => el.id === activeMarkerID)[0]?.y || ""}
                                    onChange={event => onUpdateCoordinate('y', event.target.value)}
                                />

                                <div className="input-group-append">
                                    <span className="input-group-text">%</span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <button
                        type="button"
                        className="btn btn-danger"
                        disabled={markers.length <= 3 || !activeMarkerID}
                        onClick={() => {
                            MarkersStore.deleteMarker(activeMarkerID)
                            MarkersStore.setActiveMarkerID(null)
                        }}
                    >
                        Delete marker
                    </button>
                </div>
            </div>

            <div className="card m-b-20">
                <div className="card-header">Custom Image</div>
                <div className="card-body">

                <div className="input-group mb-3">
                    <div className="custom-file">
                        <input type="file" className="custom-file-input" id="file" aria-describedby="inputGroupFileAddon03" />
                        <label className="custom-file-label" htmlFor="file">Choose image</label>
                    </div>
                </div>

                </div>
            </div>


        </aside>
    )
}

export default observer(Sidebar)