import React from 'react'
import { observer } from 'mobx-react'
import { generateColorfulClipPathString } from 'utils/clipPath.utils'
import MarkersStore from 'stores/MarkersStore'

const CodeDashboard = props => {
    const { markers } = MarkersStore

    return (
        <div className="code-dashboard ">
            <pre dangerouslySetInnerHTML={{ __html: `clip-path: ${generateColorfulClipPathString(markers)}` }}></pre>
        </div>
    )
}

export default observer(CodeDashboard)