import React, { useEffect, useRef, useState } from 'react'
import { generateClipPathString } from 'utils/clipPath.utils'
import { getRandomColor } from 'utils/randomColor.utils'
import { observer } from 'mobx-react'
import MarkersStore from 'stores/MarkersStore'


interface PoligonProps {

}

const Poligon: React.FC<PoligonProps> = props => {
    const { markers } = MarkersStore

    const poligonRef = useRef(null)
    const [dragMarkerID, setDragMarkerID] = useState(null)

    useEffect(() => {
        document.addEventListener('mousemove', handleMoveMouse)
        document.addEventListener('mouseup', handleMouseUpOnMarker)

        return () => {
            document.removeEventListener('mousemove', handleMoveMouse)
            document.removeEventListener('mouseup', handleMouseUpOnMarker)
        }
    })

    const handleDoubleClick = event => {
        event.preventDefault()

        const x = event.clientX - event.target.getBoundingClientRect().left
        const y = event.clientY - event.target.getBoundingClientRect().top

        MarkersStore.addNewMarker({
            id: Date.now(),
            color: getRandomColor(),
            x: Math.floor(x * 100 / event.target.getBoundingClientRect().width),
            y: Math.floor(y * 100 / event.target.getBoundingClientRect().height),
        })
    }

    const handleMoveMouse = event => {
        if(dragMarkerID) {
            let x = (event.clientX - poligonRef.current.getBoundingClientRect().left) * 100 / poligonRef.current.getBoundingClientRect().width
            let y = (event.clientY - poligonRef.current.getBoundingClientRect().top) * 100 / poligonRef.current.getBoundingClientRect().height
            
            if(x > 100) {
                x = 100
            }
            else if(x < 0) {
                x = 0
            }
    
            if(y > 100) {
                y = 100
            }
            else if(y < 0) {
                y = 0
            }

            MarkersStore.updateMarker({
                id: dragMarkerID, 
                x: Math.floor(x),
                y: Math.floor(y)
            })
        }
    }

    const handleMouseDownOnMarker = (id: number) => {
        MarkersStore.setActiveMarkerID(id)
        setDragMarkerID(id)
    }

    const handleMouseUpOnMarker = () => {
        setDragMarkerID(null)
    }

    return (
        <div className="poligon" onDoubleClick={handleDoubleClick} ref={poligonRef}>
            <div
                className="poligon__area"
                style={{ backgroundImage: 'url(/img/bg.jpg)', clipPath: generateClipPathString(markers) }}>
            </div>

            {markers.length && markers.map(el => (
                <div
                    onMouseDown={e => handleMouseDownOnMarker(el.id)}
                    key={el.id}
                    className="marker"
                    style={{
                        backgroundColor: el.color,
                        top: `${el.y}%`,
                        left: `${el.x}%`
                    }}
                >
                    <span className="marker__tip"></span>
                </div>
            ))}
        </div>
    )
}

export default observer(Poligon)