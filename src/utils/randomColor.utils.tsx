export const getRandomColor = () => {
    const SYMBOLS = '0123456789ABCDEF'
    let color = '#'

    for (var i = 0; i < 6; i++) {
        color += SYMBOLS[Math.floor(Math.random() * 16)]
    }

    return color
}
  