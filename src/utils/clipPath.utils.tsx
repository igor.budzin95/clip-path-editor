const MIN_MARKERS = 3


interface IGnerateClipPathString {
    (markersArray: Array<{ id: number, x: number, y: number }>): string
}

export const generateClipPathString: IGnerateClipPathString = markersArray => {   
    if(MIN_MARKERS > markersArray.length) {
        return 'none'
    }

    const markerStrings = markersArray.map(el => {
        return `${el.x}% ${el.y}%`
    })

    return `polygon(${markerStrings.join(', ')})`
}


interface IGenerateColorfulClipPathString {
    (markersArray: Array<{ id: number, x: number, y: number, color: string }>): string
}

export const generateColorfulClipPathString: IGenerateColorfulClipPathString = markersArray => {
    if(MIN_MARKERS > markersArray.length) {
        return 'none'
    }

    const markerStrings = markersArray.map(el => {
        return `<span style="color: ${el.color}">${el.x}% ${el.y}%</span>`
    })

    return `polygon(${markerStrings.join(', ')})`
}