import { makeObservable, observable, action, toJS  } from 'mobx'

const DEFAULT_MARKERS = [
    { id: 1, x: 0, y: 57, color: '#12A67C' },
    { id: 2, x: 95, y: 12, color: '#DCD48B' },
    { id: 3, x: 78, y: 95, color: '#ff0000' }
]

class MarkersStore {
    markers = DEFAULT_MARKERS
    activeMarkerID = null

    constructor() {
        makeObservable(this, {
            markers: observable,
            activeMarkerID: observable,
            addNewMarker: action,
            updateMarker: action,
            setActiveMarkerID: action,
            deleteMarker: action
        })
    }

    addNewMarker(marker) {
        this.markers.push(marker)
    }

    updateMarker(updatedProperties) {
        this.markers = this.markers.map(el => {
            return el.id === updatedProperties.id ? { ...el, ...updatedProperties} : el
        })
    }

    setActiveMarkerID(id: number|null) {
        this.activeMarkerID = id
    }

    deleteMarker(id: number) {
        this.markers = this.markers.filter(el => el.id !== id)
    }
}

export default new MarkersStore()